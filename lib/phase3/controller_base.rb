require_relative '../phase2/controller_base'
require 'active_support'
require 'active_support/core_ext'
require 'erb'
require 'byebug'

module Phase3
  class ControllerBase < Phase2::ControllerBase
    # use ERB and binding to evaluate templates
    # pass the rendered html to render_content
    def render(template_name)
      controller_name = self.class.to_s.underscore
      file_path = "views/#{controller_name}/#{template_name}.html.erb"
      file_contents = File.read(file_path)
      erb_template = ERB.new(file_contents)
      erb_results = erb_template.result(binding)
      render_content(erb_results, "text/html")
    end
  end
end
