require 'uri'
require 'byebug'
#require 'active_support'
module Phase5
  class Params
    # use your initialize to merge params from
    # 1. query string
    # 2. post body
    # 3. route params
    #
    # You haven't done routing yet; but assume route params will be
    # passed in as a hash to `Params.new` as below:
    def initialize(req, route_params = {})
      @params = {}
      return if req.query_string.nil? && req.body.nil? && route_params.empty?

      query_params = req.query_string.nil? ? {} : parse_www_encoded_form(req.query_string)
      body_params = req.body.nil? ? {} : parse_www_encoded_form(req.body)

      @params = route_params.merge(body_params).merge(query_params)
    end

    def [](key)
      @params[key.to_s]
    end

    # this will be useful if we want to `puts params` in the server log
    def to_s
      @params.to_s
    end

    class AttributeNotFoundError < ArgumentError; end;

    private
    # this should return deeply nested hash
    # argument format
    # user[address][street]=main&user[address][zip]=89436
    # should return
    # { "user" => { "address" => { "street" => "main", "zip" => "89436" } } }
    def parse_www_encoded_form(www_encoded_form)
      form_data = URI::decode_www_form(www_encoded_form)
      array_of_hashes = []
      form_hash = {}
      form_data.each do |param|
        parsed_key = parse_key(param.first.to_s)
        nested_key = sort_key(parsed_key, param.last)

        array_of_hashes << nested_key
      end

      array_of_hashes.each do |hash|
        form_hash = deep_merge(form_hash, hash)
      end
      form_hash

    end

    # this should return an array
    # user[address][street] should return ['user', 'address', 'street']
    def parse_key(key)
      parsed_key = key.split(/\]\[|\[|\]/)
    end

    def sort_key(key_array, value)
      return { key_array.first => value } if key_array.length == 1
      key_hash = {}
      key_hash[key_array.shift] = sort_key(key_array, value)
      key_hash
    end

    def deep_merge(hash1, hash2)
      if hash1.values.all? { |value| value.class != Hash } &&
              hash2.values.all? { |value| value.class != Hash }
        return hash1.merge(hash2)
      end
      hash1_keys = hash1.keys
      hash2_keys = hash2.keys
      hash1_keys.each do |key_name|
        if hash2_keys.include?(key_name) && hash2[key_name].class == Hash
          hash2[key_name] = deep_merge(hash1[key_name], hash2[key_name])
        end
      end
      merged_hash = hash1.merge(hash2)
    end

  end
end
