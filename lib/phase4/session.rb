require 'json'
require 'webrick'
require 'byebug'

module Phase4
  class Session
    # find the cookie for this app
    # deserialize the cookie into a hash
    def initialize(req)
      cookies = req.cookies
      @session_info = Hash.new { |hash, key| hash[key] = 0 }
      cookies.each do |cookie|
        if cookie.name.downcase == '_rails_lite_app'
          @session_info = JSON.parse(cookie.value)
          #byebug
        end
      end
    end

    def [](key)
      @session_info[key]
    end

    def []=(key, val)
      @session_info[key] = val
    end

    # serialize the hash into json and save in a cookie
    # add to the responses cookies
    def store_session(res)
      new_cookie = @session_info.to_json
      res.cookies << WEBrick::Cookie.new('_rails_lite_app', new_cookie)
      self
    end
  end
end
