class Cat
  def sleep
    "zz"
  end

  def pur
    "pur"
  end

  def meow
    "meow"
  end

  def plan_day_hardcoded
    sleep
    pur
    meow
    sleep
    pur
  end

  def plan_day(&blk)
    instance_eval(&blk)
  end
end

c = Cat.new
c.sleep
c.pur
c.meow
c.sleep
c.pur

c.plan_day do
  sleep
  pur
  meow
  sleep
  pur
end
