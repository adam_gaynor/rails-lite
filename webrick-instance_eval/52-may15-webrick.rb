require 'webrick'
require 'byebug'

server = WEBrick::HTTPServer.new(:Port => 3000)
template = "<h1>my favorite cat is <%= favorite_cat %>! :3</h1>"

server.mount_proc("/") do |req, res|
  favorite_cat = "Sennacy"

  if req.query["cat"]
    favorite_cat = req.query["cat"]
    res.cookies << WEBrick::Cookie.new("CAT_APP", favorite_cat)
  elsif req.path == "/google"
    res.status = 302
    res["Location"] = "https://www.google.com"
  else
    cookie = req.cookies.find { |c| c.name == "CAT_APP" }
    favorite_cat = cookie.value
  end

  res.body = ERB.new(template).result(binding)
end

trap('INT') { server.shutdown } # what to do when you hit CTRL+C

server.start
